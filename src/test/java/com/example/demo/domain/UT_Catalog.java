/*
 * Algebra labs.
 */
package com.example.demo.domain;

import java.util.Collection;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.example.demo.service.Catalog;
import org.junit.Assert;

public class UT_Catalog {

    @Test
    public void catalogTest() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        Assert.assertTrue("spring container should not be null", ctx != null);

        // Look up catalog, make sure it is non-null and output it
        Catalog cat = ctx.getBean(Catalog.class);
        Assert.assertTrue("Catalog should be non-null", cat != null);
        System.out.println(cat);

        // Check that the catalog has some elements now
        Assert.assertTrue("Catalog size should not be zero", cat.size() != 0);

        // Look up some items, and output them.
        Collection<MusicItem> results = cat.findByKeyword("a");
        System.out.println("*** Results for lookup on 'a' ***");
        for (MusicItem cur : results) {
            System.out.println(cur);
        }

        ctx.close();
    }

}
